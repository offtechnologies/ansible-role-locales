ansible-role-locales
=========
[![pipeline status](https://gitlab.com/offtechnologies/ansible-role-locales/badges/master/pipeline.svg)](https://gitlab.com/offtechnologies/ansible-role-locales/commits/master)

[offtechurl]: https://gitlab.com/offtechnologies

[![offtechnologies](https://gitlab.com/offtechnologies/logos/raw/master/logo100.png)][offtechurl]

Sets locales in Debian-like systems.

Requirements
------------
None

Suported Platforms
--------------
Debian 9 (Stretch)

Role Variables
--------------

see `defaults/main.yml` and `vars/main.yml`

Dependencies
------------

None

Example Playbook
----------------

```yaml
---

- name: Configure dev system(s)
  hosts: workstations
  become: True
  roles:
    - { role: ansible-role-locales}
```

License
-------

BSD
